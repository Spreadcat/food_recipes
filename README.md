# food_recipes

This is my personal collection of food recipes.

Some of them come from a binder I got to my 18th birthday with a collection of recipes, others I've collected myself.

Feel free to contribute or make improvements.

---

## Breads

* [Banana Bread with walnuts (EN)][#001]
* [Best Banana bread (EN)][#058]
* [Cheese bread (DE)][#020]
* [Norwegian Pawn-bread][#031]
* [Scones (DE)][#047]
* [Shortbread fingers (DE)][#048]

---

## Cakes

* [Apple pie, fine (DE)][#012]
* [Apple pie without milk (NO)][#011]
* [Cherry cake (NO)][#024]
* [Rharbarbar cake (DE)][#045]
* [Rhubarb-Meringues Cake (EN)][#061]
* [Saturday coziness (NO)][#023]
* [Snikkers Cake (NO)][#039]

---

## Cookies and sweets

* [Coconut Slice and bake cookies (EN)][#006]
* [Cookie Amaretti (EN)][#007]
* [Sjoko Duo Cookies (NO)][#038]
* [Snails (DE)][#046]
* [The best chocolate chip cookies (EN)][#041]
* [The best chocolate chip cookies (DE)][#059]

---

## Desserts

* [Apple pie without milk (NO)][#011]
* [Apple pie, fine (DE)][#012]
* [Cherry cake (NO)][#024]
* [Saturday coziness (NO)][#023]
* [Snikkers Cake (NO)][#039]
* [Krumkake, eastfrisian][#063]
* [Krumkake, nothern-german][#065]

---

## Main courses

* [Bresso-Hühnchen (DE)][#002]
* [Canneloni, gefüllt (DE)][#004]
* [Casserole, potatoes and vegetables (DE)][#025]
* [Chicken curry, Indian (NO)][#019]
* [Chinapan (DE)][#005]
* [Curry Curry Nam Nam (NO)][#009]
* [Frikadellen, gerste (DE)][#013]
* [Green Cabbage (DE)][#035]
* [Pasta and green cabbage pesto (NO)][#042]
* [Pizza (DE)][#043]
* [Pizza with minced meat (DE)][#016]
* [Pizza Napoli (EN)][#064]
* [Porresoup with Champignons (DE)][#044]
* [Potato-pieces (DE)][#026]
* [Scampigryte med rød karri (NO)][#037]
* [Spaghetti in Meatballs (EN)][#049]
* [Waffles, Norwegian (EN)][#060]
* [Wok with pig-meat (NO)][#056]
* [Zucciniburger (NO)][#057]
* [Fårikål (EN)][#062]

---

## Sides

* [Chicken Wings, Buffalo (NO)][#003]
* [Chicken wings, crispy (EN)][#008]
* [Chicken wings, garlic parmesan (EN)][#018]
* [Chicken wings, honey BBQ (EN)][#017]
* [Dipp (DE)][#010]
* [Teriyaki chicken wings (NO)][#054]

---

## Sauces

* [Teriyaki sauce (NO)][#055]

---

## Soups

* [Asparagus creme soup (DE)][#050]
* [Dipp (DE)][#010]
* [Soup with taste (DE)][#052]
* [Star soup (DE)][#051]
* [Turkish soup (DE)][#053]

---

## Information

* If not specified, the recipe is in English. Norwegian and German might also occur, but should be listed as such.
* The following abbreviations are used (for English):

  Abbr. | Unit | Comment
  -- | -- | --
  dr | Drop | 0.05 ml
  gr | Gram | -
  l | Liter | 1000 ml
  ml | Milliliter |  1 ml
  pc | Piece |
  tbsp | Tablespoon | 14.7868
  ts | Teaspoon | 4.92892 ml

[#001]: ./breads/bread-banana_bread_with_walnuts.mkd
[#002]: ./main_courses/bressohuenchen.mkd
[#003]: ./sides/buffalo_kylling_vinger.mkd
[#004]: ./main_courses/canneloni_gefuellt.mkd
[#005]: ./main_courses/china-pfanne.mkd
[#006]: ./cookies/cookies_coconut_slice_and_bake.mkd
[#007]: ./cookies/cookies_amaretti.mkd
[#008]: ./sides/crispy_chicken_wings.mkd
[#009]: ./main_courses/curry_curry_nam_nam.mkd
[#010]: ./sides/dipp.mkd
[#011]: ./cakes/cake_eplekake_uten_melk.mkd
[#012]: ./cakes/cake_feiner_apfelkuchen.mkd
[#013]: ./main_courses/gersten_frikadellen.mkd
[#014]: ./soups/gewuerzsuppe_mit_tofu.mkd
[#015]: ./soups/gyrossuppe.mkd
[#016]: ./main_courses/hackfleischpizza.mkd
[#017]: ./sides/honnig_bbq_kylling_vinger.mkd
[#018]: ./sides/hvitlok_parmesan_kylling_vinger.mkd
[#019]: ./main_courses/indisk_kyllingcurry.mkd
[#020]: ./breads/bread_cheese.mkd
[#021]: ./soups/kæse-kræuter-suppe.mkd
[#022]: ./soups/kaesesuppe.mkd
[#023]: ./cakes/cake_lordagskos.mkd
[#024]: ./cakes/cake_morellkake.mkd
[#025]: ./main_courses/kartoffel_gemuese_auflauf.mkd
[#026]: ./main_courses/kartoffelspalten.mkd
[#027]: ./soups/kartoffelsuppe.mkd
[#028]: ./desserts/krumkake_nrk.adoc
[#029]: ./desserts/krumkake_tine.mkd
[#030]: ./soups/mitternachtssuppe.mkd
[#031]: ./breads/bread_norwegian_pawnbread.mkd
[#032]: ./breads/bread_banana_2.mkd
[#033]: ./soups/soup_currysuppe_med_kylling.mkd
[#034]: ./main_courses/main_duftkuchen.mkd
[#035]: ./main_courses/main_green_cabbage.mkd
[#036]: ./desserts/krem_catalana.mkd
[#037]: ./main_courses/scampigryte_med_roed_karri.mkd
[#038]: ./cookies/cookies_sjoko_duo_cookies.mkd
[#039]: ./cakes/snikkers_kake.mkd
[#041]: ./cookies/cookies_the_best_chocolate_chip_cookies.adoc
[#042]: ./main_courses/pasta_og_groennkaalpesto.mkd
[#043]: ./main_courses/pizza.mkd
[#044]: ./main_courses/porresuppe_mit_champignons.mkd
[#045]: ./cakes/rharbarberkuchen.mkd
[#046]: ./cookies/schnecken.mkd
[#047]: ./breads/scones.mkd
[#048]: ./breads/shortbreadfingers.mkd
[#049]: ./main_courses/spaghetti_in_meatballs.mkd
[#050]: ./soups/spargelcremesuppe.mkd
[#051]: ./soups/sternchensuppe.mkd
[#052]: ./soups/suppe_mit_geschmack.mkd
[#053]: ./soups/suppe_tuerkisch.mkd
[#054]: ./sides/teriyaki_kylling_vinger.mkd
[#055]: ./sauces/teriyakisaus.mkd
[#056]: ./main_courses/wok_med_svinekjoett.mkd
[#057]: ./main_courses/zucciniburger.mkd
[#058]: ./breads/bread-best_banana_bread.adoc
[#059]: ./cookies/cookies_the_best_chocolate_chip_cookies_de.mkd
[#060]: ./main_courses/waffles_norwegian.mkd
[#061]: ./cakes/rhubarb_meringues_cake.adoc
[#062]: ./main_courses/farikal.adoc
[#063]: ./desserts/krumkake_ostfrisisk.adoc
[#064]: ./main_courses/pizza_napoli.mkd
[#065]: ./desserts/krumkake_nordtysk.adoc

