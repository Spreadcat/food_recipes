Tränenkuchen
============
:Autor: unknown
:Personer: 6
:Language: de_de

image::../images/cake_traenenkuchen.jpg[]

[width="50%"]
|=======
|Active time | 90 min
|Total time | 12 h
|Temperature | 175 dg, Ober- und Unterhitze
|Baking time | 70 min + 10 min
|Tools | Springform (26cm), Backpapier
|=======


## Zutaten

- Mehl, 200 gr
- Zucker, 250 gr
- Butter, 70 gr
- Ei, 4 stk
- Backpulver, 1 tl
- Magerquark, 500 gr
- Vanilljezucker, 1 pk
- Puddingpulver, vanilje, 1 pk
- Sahne, 250 ml
- Milch, 250 ml
- Öl, 150ml
- Puderzucker, 6 El

## Zubereitung

. **Teig**: Mehl (200gr), Zucker (100 gr), Butter (70 gr), Ei (1 Stk), Backpulver (1 Tl)
.. Die Zutaten vermengen und einen Knetteig herstellen.
.. Den Boden der Springform mit Backpapier auslegen und den Rand ausfetten.
.. Den Teig ausrollen und den Rand von Hand hochdrücken. Da der Teig sehr brüchig sein kann, die Masse in der Springform mit einem Eßlöffel in Form drücken.
. Den Backofen auf 175 grad vorheizen.

---
[start=3]
. **Belag**: Magerquark (500 gr), Zucker (150 gr), Vanillezucker (1 pk), Puddingpulver Vanille (1 pk), Ei (1 stk), Eigelb (2 Stk), Sahne (250 ml), Milch (250 ml), Öl (150 ml)
.. Den Quark kurz geschmeidig rühren.
.. Anschließend alle Zutaten nach und nach dazugeben und gut verrühren. Das Eiweiß der Eier aufheben.
.. Die sehr flüssige Masse in die Springform füllen.

---

[start=4]
. Den Kuchen 70 Minuten backen.
. Anschließend aus dem Ofen nehmen und abkühlen lassen.
+
Den Ofen nicht ausschalten.
+

---

[start=6]
. **Eischnee**: Eiweiss (2 Stk), Puderzucker (6 El)
.. Wåhrend des Abkühlens das Eiweiß mit dem Puderzucker steif schlagen. Dies wird nicht unmittelbar steif werden.
.. Den Eischnee cirka 1 cm dick auf den Bodn streichen. Die Menge müsste mehr als ausreichen.

---

[start=7]
. Den Kuchen wieder für zehn (10) Minuten im Ofen bei gleicher Hitze weiterbacken.
. Den Kuchen anschließend aus dem Ofen nehmen und etwas abkühlen lassen.
. Den Rand der Springform vorsichtig mit einem Messer lösen.
. Mehrere Stunden oder über Nacht den Kuchen auf dem Boden der Springform abkühlen lassen.
+
Dabei sollten sich feine Tropfen auf der Kuchenoberfläche bilden.
+

