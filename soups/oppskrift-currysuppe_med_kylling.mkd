# Currysuppe med Kylling, Paprika, Squash og Løk

&nbsp; | &nbsp;
--- | ---
Autor | [Trinesmatblogg](http://trinesmatblogg.no/2013/10/01/currysuppe-med-kylling-paprika-squash-og-lok/)
Personer | 3-4

---

## Ingredienser

* nøytral olje til steking
* 1 ss grønn currypasta
* 1 gul løk, finhakket
* 1 ss ingefær, revet
* 2-3 hvitløkfedd, finhakket
* 1 rød chili uten frø, finhakket
* 1 ss koriander frø
* 4 dl kokosmelk
* 4 dl kyllingkraft
* 2 lime, skall og saft
* 4 stilker sitrongress
* 1 ss brunt sukker
* 1 ss fish sauce
* 3-4 kyllingfileter, i strimler
* ca. 1 ts grønn curry
* 1 liten rød paprika, i strimler
* 1/2 squash, i strimler
* 2 sjalottløk, i ringer
* ca. 3 porsjoner nudler, tilberedt etter anvisningen på pakken
* salt og pepper
* frisk koriander eller thai basilikum

---

## Tilberedning

1. Ha litt nøytral olje i stekepannen, tilsett grønn currypasta og la den surre i et par minutter.
1. Tilsett løk og la det surret et par minutter til, før du tilsetter ingefær, hvitløk, chili og korianderfrø, og lar det surre med i ca. ett minutt.
1. Tilsett kokosmelk, kyllingkraft, limeskall og limesaft, og rør godt.
1. Legg sitrongresset på en skjærefjøl og bank den tykkeste delen med bunnen av en stekepanne eller lignende et par ganger, slik at sitrongresset blir delvis knust.
1. Ha sitrongresset i gryten sammen med sukker og fish sauce. La suppen putre på lav varme i ca. 10 minutter.
1. I mellomtiden skjærer du opp kylling, paprika, squash og løk til garnityr, slik at du har alt klart.
1. Fres kyllingen i litt nøytral olje tilsatt litt grønn curry, til den er gjennomstekt. Legg til side.
1. Fres også grønnsakene raskt i pannen og bland det hele sammen. Krydre med salt og pepper.
1. Kok nudlene etter anvisningen på pakken. Nudlene kan eventuelt erstattes med ris.
1. Sil suppen og ha den tilbake i gryten. Gi den et oppkok og smak til med salt og pepper.
1. Ha nudlene i en dyp tallerken og hell suppen over. Fordel deretter kylling- og grønnsaksgarnityret oppi tallerkenen, og strø deretter frisk koriander eller thaibasilikum over. Serveres umiddelbart.
