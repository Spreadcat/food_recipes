Matpakkebrød
============
:Autor: Ukjent
:Personer: 4
:Language: no_nb

.. image::../images/bread-the_best_banana_bread.jpg[]

[width="60%"]
|=======
|Active time | 20 min
|Total time | 60 min
|Temperature | 220 ºC
|Baking time | 40-45 min
|Tools
a|
* Plastfolie
* bakeformer
. bread baking form 25cm, ca 1.5 l
. mixer
. rubber spatula
. table spoon
|=======


## Ingredience

* Kveitemjøl sikta, 450 gr
* Kveitemjøl sammalt, 300 gr
* Havregryn, 250 gr
* Tørrgjær, 1 ts
* Salt, 2 ts
* Vann romtemperert, 1l

## Preparation

* Bland alle ingredensier i en bolle og rør sammen.
* Hell røra i to brødfomer.
* La formene stå tildekke med platforlie i 10-12 timer. Brød er klare til steking når degen har hevna opp til kanten og det bobler lett på overflaten.
* Hell over litt ekstra havegryn før baking.
* Bak på 220 grader i 40-45 minutt til brødene er gylne og har en hul lyd når du banker på undersida.
* Avkjøl på rist.

Brød holser seg saftige og gode noen dager og kan også fryses.
