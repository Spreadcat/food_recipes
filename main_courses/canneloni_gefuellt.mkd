# Canneloni, gefüllt

![Gefüllte Cannelloni](../images/cannelloni_gefuellt.jpg)

---

## Zutaten

* Canneloni
* 1 Tomato Algustu Kræueter
* 2 Mozarella
* 300 g Gorgonzola
* 1 Brokkoli
* Parmesan
* 1 L Quark, 40%

---

## Zubereitung

1. Quark, Gorgonzola und Mozarella in einer Schuessel kleinschneiden und durchruehren.
1. Brokkoli blanchieren, dazugeben und vermischen.
1. Auflaufform fetten.
1. Den Boden mit der Hælte Tomato bedecken.
1. Canneloni fuellen und einschichten.
1. Rest Tomato und Kæse darueber verteilen.
1. Abschliessend Parmesan darauf.
1. Ca. 0.5 Stunden bei 180-200 Grade im Ofen garen.
