= Nordtyske Krumkaker

:Autor: Gertrud Cramer
:Personer: 0
:Stykk: 20 krumkaker
:language: no_nb

image::../images/krumkake.jpg[]

[width="50%"]
|====
| Active Time | 13t, 60 min baking
| Total time | 120min
| Persons | ?
| Verktøy | Baking jern, gummi skje, 1 gaffel, 1 skje
|====

---

== Ingrediensier

* Sukker, 250g
* Vann, 750ml
* Smør, 200g
* Egg, 2 stk
* Mel, 400g
* Salt, 0.5ts
* Anis, 0.5ts
* Kanel, 0.5ts

== Tilberedning

* Løs opp sukker i varmt vann og la vannet kjøle ned til romtemperatur.
* Rør smør og egg i en bolle til skum.
* Bland inn mel, anis (opsjonal), kanel og salt.
* Bland in den kalde sukkervann-blandingen med en skje inni deigen. Bland godt. Viktig: Deigen må være veldig flytende, bland inn mer vann hvis det trenges.
* La deigen stå i sirka 12 timer.
* Bak deigen på krumkakejern.

== Mer tipps

* Flere krumkakejern øker produksjonen. For mange senker den.
